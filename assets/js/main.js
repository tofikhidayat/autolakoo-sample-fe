jQuery(document).ready(function($) {
		setTimeout(function(e) {
	    $(".loader").fadeOut();
	}, 400)
});


	$(window).scroll(function(event) {
	    if ($(this).scrollTop() > 100) {
	        $("header").addClass('scroll');
	    } else {
	        $("header").removeClass('scroll');
	    }
	});


	AOS.init({
		 duration: 400,
		 easing:"linear",
	});


	$("#carousel-client").owlCarousel({
	    loop: true,
	    margin: 10,
	    nav: true,
	    items: 3,
	    navigation: true, ///show hide navigation 
	    navText: ['', ''], //untuk mengatur navisagti test bisa sa diganti dengan fontawesome 
	    autoplay: false,
	    autoplayTimeout: 1000,
	    autoplayHoverPause: true,
	    responsive: {
	        // ini ungtuk mengatur responsive 
	        0: {
	            items: 1
	        },
	        768: {
	            items: 2
	        },
	        1000: {
	            items: 3
	        }
	    }
	});





$(document).ready(function(){

  // Add smooth scrolling to all links
  $("a.go").on('click', function(event) {

    $(this).closest('ul').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});



$(".pay").click(function(e){
	$("#modal-email").modal("show")
})